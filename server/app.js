const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const chalk = require('chalk');
const initDatabase = require('./startUp/initDatabase')
const router = require('./routes')


const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api', router);

const PORT = config.get('port') ?? 8080;

// if (process.env.NODE_ENV !== 'production') {
//   console.log(chalk.green('Running in development mode'));
// } else {
//   console.log(chalk.red('Running in production mode'));
// }

async function start() {
  try {
    mongoose.connection.once('open', () => {
      initDatabase();
    })
    await mongoose.connect(config.get('mongoURI'))
    app.listen(PORT, () => {
      console.log(chalk.green(`Server started on port ${PORT}`));
    })
  } catch (err) {
    console.log(chalk.red(err.message));
    process.exit(1);
  }

}


start()
