const express = require('express');
const User = require("../models/User");
const {check, validationResult} = require('express-validator');
const bcrypt = require('bcryptjs');
const {generateUserData} = require("../utils/helpers");
const router = express.Router({mergeParams: true});
const tokenService = require("../services/token.service");

const MIN_PASSWORD_LENGTH = 6;

const signUpValidation = [
    check('email', 'Некорректный email').isEmail(),
    check('password', `Минимальная длина пароля ${MIN_PASSWORD_LENGTH} символов`).isLength({min: MIN_PASSWORD_LENGTH}),
]

const signInValidation = [
    check('email', 'Некорректный email').normalizeEmail().isEmail(),
    check('password', `Пароль не может быть пустым`).exists(),
]

router.post('/signUp', [
    ...signUpValidation,
    async (req, res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    error: {
                        message: 'INVALID_DATA',
                        code: 400,
                        errors: errors.array()
                    }
                });
            }
            const {email, password} = req.body

            const user = await User.findOne({email})
            if (user) {
                return res.status(400).json({
                    error: {
                        message: "EMAIL_EXISTS",
                        code: 400
                    }
                })
            }

            const hashedPassword = await bcrypt.hash(password, 12)

            const newUser = await User.create({
                ...req.body,
                ...generateUserData(),
                password: hashedPassword
            })

            const tokens = tokenService.generate({_id: newUser._id});
            await tokenService.save(newUser._id, tokens.refreshToken);

            return res.status(201).send({...tokens, userId: newUser._id})

        } catch (e) {
            res.status(500).json(
                {message: "C Auth что-то не так"});
        }
    }])

router.post('/signInWithPassword', [
    ...signInValidation,
    async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    error: {
                        message: 'INVALID_DATA',
                        code: 400,
                        errors: errors.array()
                    }
                });
            }

            const {email, password} = req.body

            const user = await User.findOne({email})

            if (!user) {
                return res.status(400).json({
                    error: {
                        message: "USER_NOT_FOUND",
                        code: 400
                    }
                })
            }

            const isPasswordValid = await bcrypt.compare(password, user.password)

            if (!isPasswordValid) {
                return res.status(400).json({
                    error: {
                        message: "INVALID_PASSWORD",
                        code: 400
                    }
                })
            }

            const tokens = tokenService.generate({_id: user._id});
            await tokenService.save(user._id, tokens.refreshToken);

            res.status(200).json({...tokens, userId: user._id})


        } catch (e) {
            res.status(500).json(
                {message: "C Auth signInWithPassword что-то не так"});
        }
    }])

router.post('/token', async (req, res) => {
    try {
        const {refresh_token: refreshToken} = req.body;
        const dataToken = tokenService.validateRefreshToken(refreshToken);
        const dbToken = await tokenService.findToken(refreshToken)

        if (!dataToken || !dbToken || dataToken._id !== dbToken?.user?.toString()) {
            return res.status(401).json({message: 'Unauthorized'})
        }

        const tokens = tokenService.generate({_id: dbToken.user});
        await tokenService.save(dbToken.user, tokens.refreshToken);

        res.status(200).json({...tokens, userId: dbToken.user})

    } catch (e) {
        res.status(500).json(
            {message: "C Auth token что-то не так"});
    }
})

module.exports = router;
