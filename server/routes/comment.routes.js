const express = require('express');
const router = express.Router({mergeParams: true});
const Comment = require('../models/Comment');
const auth = require('../middleware/auth.middleware');


router.get('/', auth, async (req, res) => {
    try {
        const {orderBy, equalTo} = req.query
        const list = await Comment.find({[orderBy]: equalTo});
        res.status(200).send(list);
    } catch (e) {
        res.status(500).json(
            {message: "На сервере произошла ошибка, попробуйте позже"});
    }
})

router.post('/', auth, async (req, res) => {
    try {
        const newComment = await Comment.create({
            ...req.body,
            userId: req.user._id
        })
        res.status(201).send(newComment);
    } catch (e) {
        res.status(500).json(
            {message: "На сервере произошла ошибка, попробуйте позже"});
    }
})

router.delete('/:commentId', auth, async (req, res) => {
    try {
        const {commentId} = req.params;
        const commentForRemove = await Comment.findById(commentId)

        if (commentForRemove.userId === req.user._id) {
            await commentForRemove.remove();
            res.send(null)
        } else {
            return res.status(401).json({message: 'Unauthorized'});
        }
    } catch (e) {
        res.status(500).json(
            {message: "На сервере произошла ошибка, попробуйте позже"});
    }
})

module.exports = router;
