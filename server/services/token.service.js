const jwt = require('jsonwebtoken');
const config = require('config');
const Token = require('../models/token');

class TokenService {
    generate(payload) {
        const accessToken = jwt.sign(payload, config.get("accessSecret"), {
            expiresIn: 3600
        })

        const refreshToken = jwt.sign(payload, config.get("refreshSecret"))

        return {
            accessToken,
            refreshToken,
            expiresIn: 3600
        }
    }

    async save(userId, refreshToken) {
        const data = await Token.findOne({user: userId})
        if (data) {
            data.refreshToken = refreshToken
            await data.save()
        }

        const token = await Token.create({user: userId, refreshToken})
        return token
    }

    validateRefreshToken(refreshToken) {
        try {
            return jwt.verify(refreshToken, config.get("refreshSecret"))
        } catch (e) {
            return null
        }
    }

    validateAccessToken(accessToken) {
        try {
            return jwt.verify(accessToken, config.get("accessSecret"))
        } catch (e) {
            return null
        }
    }

    async findToken(refreshToken) {
        try {
            return await Token.findOne({refreshToken})
        } catch (e) {
            return null
        }
    }

}

module.exports = new TokenService();
